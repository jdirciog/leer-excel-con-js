window.addEventListener('load', () => {
    let inputFile = document.getElementById('excel-file'),
        buttonToHtml = document.getElementById('result-to-html'),
        buttonToJson = document.getElementById('result-to-json'),
        resultContainer = document.getElementById('result-container'),
        file = {};

    buttonToHtml.disabled = true;
    buttonToJson.disabled = true;

    inputFile.addEventListener('change', (e) => {
        console.log('> new file');

        resultContainer.innerHTML = '';
        file = e.target.files[0];
        buttonToHtml.disabled = false;
        buttonToJson.disabled = false;
    });

    buttonToHtml.addEventListener('click', resultHtml);
    buttonToJson.addEventListener('click', resultJson);

    function resultHtml() {
        console.log('> Result Html');
        let reader = new FileReader();

        reader.readAsArrayBuffer(file);
        reader.onload = () => {
            let data = new Uint8Array(reader.result),
                workbook = XLSX.read(data, { type: 'array' }),
                result = XLSX.write(workbook, {
                    type: 'binary', bookType: 'html'
                });

            resultContainer.innerHTML = result;
        }
    }

    function resultJson() {
        console.log('> Result Json');
        let reader = new FileReader();

        reader.readAsBinaryString(file);
        reader.onload = e => {
            let data = e.target.result,
                workbook = XLSX.read(data, {
                    type: 'binary', cellDates: true, cellText: false
                }),
                firstSheetName = workbook.SheetNames[0],
                XLRowObject = XLSX.utils.sheet_to_json(workbook.Sheets[firstSheetName], { raw: false, dateNF: 'YYYY-MM-DD' }),
                result = JSON.stringify(XLRowObject);

            resultContainer.innerHTML = result;
        }
    }
});